

package com.cloudera.flume.source;
import java.util.HashMap;
import java.util.Map;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.AbstractSource;

import org.apache.log4j.Logger;

import sun.security.util.BigInt;
import twitter4j.*;

import twitter4j.conf.ConfigurationBuilder;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.json.DataObjectFactory;


public class TwitterInterns extends AbstractSource
        implements EventDrivenSource, Configurable {

    private static final Logger logger =
            Logger.getLogger(TwitterInterns.class);


    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;
    private String[] keywords;

    private long[] twitterid= new long[]{1016076290, 115044590, 507672047, 742143, 64643056, 69008563, 26235265, 36339032, 226087776};
    long l=Long.MAX_VALUE;

    private TwitterStream twitterStream;


    @Override
    public void configure(Context context) {
        consumerKey = context.getString(TwitterSourceConstants.CONSUMER_KEY_KEY);
        consumerSecret = context.getString(TwitterSourceConstants.CONSUMER_SECRET_KEY);
        accessToken = context.getString(TwitterSourceConstants.ACCESS_TOKEN_KEY);
        accessTokenSecret = context.getString(TwitterSourceConstants.ACCESS_TOKEN_SECRET_KEY);


        String keywordString = context.getString(TwitterSourceConstants.KEYWORDS_KEY, "");
        if (keywordString.trim().length() == 0) {
            keywords = new String[0];
        } else {
            keywords = keywordString.split(",");
            for (int i = 0; i < keywords.length; i++) {
                keywords[i] = keywords[i].trim();
            }
        }
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(consumerKey);
        cb.setOAuthConsumerSecret(consumerSecret);
        cb.setOAuthAccessToken(accessToken);
        cb.setOAuthAccessTokenSecret(accessTokenSecret);
        cb.setJSONStoreEnabled(true);
        cb.setIncludeEntitiesEnabled(true);

        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
    }

    @Override
    public void start() {

        final ChannelProcessor channel = getChannelProcessor();

        final Map<String, String> headers = new HashMap<String, String>();

        StatusListener listener = new StatusListener() {
            public void onStatus(Status status) {
                if(status.isRetweet()==false) {
                    JSONObject jsonObject = new JSONObject();
                    logger.info(status.getUser().getScreenName() + ": " + status.getText() + "\nstatus =" + status + "\n");
                    try {
                        jsonObject.put("status", status.getText());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    headers.put("timestamp", String.valueOf(status.getCreatedAt().getTime()));
                    Event event = null;
                    String str = status.getCreatedAt()+" "+status.getText();
                    event = EventBuilder.withBody(str.getBytes(), headers);


                    channel.processEvent(event);
                }
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }

            public void onScrubGeo(long userId, long upToStatusId) {
            }

            public void onException(Exception ex) {
            }

            public void onStallWarning(StallWarning warning) {
            }
        };

        twitterStream.addListener(listener);
        twitterStream.sample();
        FilterQuery query = new FilterQuery().follow(twitterid);
        twitterStream.filter(query);

        super.start();
    }


    @Override
    public void stop() {

        twitterStream.shutdown();
        super.stop();
    }
}
